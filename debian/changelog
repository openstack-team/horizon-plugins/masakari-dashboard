masakari-dashboard (11.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090434).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 16:14:00 +0100

masakari-dashboard (11.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2024 17:02:16 +0200

masakari-dashboard (11.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Sep 2024 13:58:26 +0200

masakari-dashboard (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2024 10:10:37 +0200

masakari-dashboard (10.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Apr 2024 21:30:21 +0200

masakari-dashboard (10.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed django-4-ugettext_lazy-is-removed.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Mar 2024 15:20:44 +0100

masakari-dashboard (9.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 11:53:33 +0200

masakari-dashboard (9.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Reintroduce django-4-ugettext_lazy-is-removed.patch that is somehow not in
    the upstream code anymore (Closes: #1052757).

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 11:24:51 +0200

masakari-dashboard (9.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Sep 2023 10:47:36 +0200

masakari-dashboard (8.0.0-4) unstable; urgency=medium

  * Cleans better (Closes: #1047431).

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Aug 2023 12:11:00 +0200

masakari-dashboard (8.0.0-3) unstable; urgency=medium

  * Removed build-depends on python3-openstack.nose-plugin.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Aug 2023 11:32:52 +0200

masakari-dashboard (8.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 16:35:02 +0200

masakari-dashboard (8.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 15:36:47 +0100

masakari-dashboard (8.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Mar 2023 17:32:33 +0100

masakari-dashboard (7.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Oct 2022 22:39:26 +0200

masakari-dashboard (7.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Sep 2022 04:15:17 +0200

masakari-dashboard (7.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed Django 4 compat patches, fixed upstream.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Sep 2022 09:14:50 +0200

masakari-dashboard (6.0.0-2) unstable; urgency=medium

  * Add Django 4 compat patches (Closes: #1015104):
    - django-4-django.conf.urls.url-is-removed.patch
    - django-4-ugettext_lazy-is-removed.patch

 -- Thomas Goirand <zigo@debian.org>  Sat, 30 Jul 2022 10:31:29 +0200

masakari-dashboard (6.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 17:25:08 +0200

masakari-dashboard (6.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Mar 2022 13:57:18 +0200

masakari-dashboard (6.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Mar 2022 14:34:17 +0100

masakari-dashboard (5.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:20:24 +0200

masakari-dashboard (5.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:51:12 +0200

masakari-dashboard (5.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * (build-)depends on horizon version >= 3:20.0.0+git2020.09.21.27036cc0eb.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Sep 2021 09:07:17 +0200

masakari-dashboard (4.0.0-3) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 16:18:52 +0200

masakari-dashboard (4.0.0-2) experimental; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.
  * Add Breaks: python3-django-horizon (<< 3:19.2.0-2~).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 May 2021 16:30:46 +0200

masakari-dashboard (4.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 16:43:26 +0200

masakari-dashboard (4.0.0~rc1-1) experimental; urgency=medium

  * Initial packaging (Closes: #986466).

 -- Thomas Goirand <zigo@debian.org>  Sat, 27 Mar 2021 21:37:04 +0100
